package BookstoreProject;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;

public class JDBCMainWindow extends JFrame implements ActionListener
	{
		String cmd = null;

		// DB Connectivity Attributes
		private Connection con = null;
		private Statement stmt = null;
		private ResultSet rs = null;
		
		private JMenuItem exitItem;
		private JMenuItem BookExport;
		private JMenuItem EmployeeExport;
		private JTabbedPane TabPanel;
		private JMenuBar menuBar;
		
		JDBCMainWindowContent Window1 = new JDBCMainWindowContent( "JDBC Books Management System");
		JDBCEmployeeWindow Window2 = new JDBCEmployeeWindow("Emplpoyee Management System");
		public JDBCMainWindow()
		{
			// Sets the Window Title
			super( "Books Management" ); 
			
			//Setup fileMenu and its elements
			menuBar=new JMenuBar();
			JMenu fileMenu=new JMenu("File");
			exitItem =new JMenuItem("Exit");
			TabPanel = new JTabbedPane();
	
			fileMenu.add(exitItem);
			menuBar.add(fileMenu );
			setJMenuBar(menuBar);
			
			JMenu mnExport = new JMenu("Export");
			menuBar.add(mnExport);
			
			EmployeeExport = new JMenuItem("Export Employee List");
			mnExport.add(EmployeeExport);
			
			BookExport = new JMenuItem("Export Book List");
			mnExport.add(BookExport);
			
			// Add a listener to the Exit Menu Item
			exitItem.addActionListener(this);
			BookExport.addActionListener(this);
			EmployeeExport.addActionListener(this);

			// Create an instance of our class JDBCMainWindowContent 
			
			// Add the instance to the main section of the window
			TabPanel.add("Details", Window1 );
			TabPanel.add("Employee", Window2);
			TabPanel.add("Functions & Export", Window1.ExportPanel);
			getContentPane().add( TabPanel );
			
			setSize( 1125, 550 );
			setVisible( true );
		}

		// The event handling for the main frame
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource().equals(exitItem)){
				this.dispose();
			}
			if(e.getSource().equals(BookExport)){
				cmd = "select * from details;";

				try{					
					rs= stmt.executeQuery(cmd); 	
					Window1.writeToFile(rs);
				}
				catch(Exception e1){e1.printStackTrace();}
			}
			if(e.getSource().equals(EmployeeExport)){
				this.dispose();
			}
		}
		
		
		
	}