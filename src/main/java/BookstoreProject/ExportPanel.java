package BookstoreProject;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class ExportPanel {

	JFrame frame;
	JPanel exportButtonPanel;
	private JButton  NumOfBooks = new JButton("Total Number of Books:");
	private JTextField NumOfBooksTF  = new JTextField(12);
	private JButton avgAgeDepartment  = new JButton("AvgAgeForDepartment");
	private JTextField avgAgeDepartmentTF  = new JTextField(12);
	private JButton ListAllDepartments  = new JButton("ListAllDepartments");
	private JButton ListAllPositions  = new JButton("ListAllPositions");
	private JButton ListMorning = new JButton("Morning Shift: ");
	private JButton ListAfternoon = new JButton("Afternoon Shift: ");
	private JButton ListEvening = new JButton("Evening Shift: ");
	private static QueryTableModel TableModel2 = new QueryTableModel();
	private JTable TableofDBContents2=new JTable(TableModel2);
	private JScrollPane dbContentsPanel2;
	private Border lineBorder;
	JTextField TF=new JTextField(10);
	 private final JTextField textField = new JTextField(12);

	public ExportPanel() {
		frame = new JFrame();
		frame.setTitle("Something");
		Container cp = frame.getContentPane();
		cp.setLayout(new BorderLayout());
		JPanel panel=new JPanel();
		
		exportButtonPanel=new JPanel();
		exportButtonPanel.setLayout(new GridLayout(9,1));
		exportButtonPanel.setBackground(Color.lightGray);
		exportButtonPanel.setBorder(BorderFactory.createTitledBorder("Export Data"));
		exportButtonPanel.add(NumOfBooks);
		exportButtonPanel.add(NumOfBooksTF);
		exportButtonPanel.add(avgAgeDepartment);
		exportButtonPanel.add(avgAgeDepartmentTF);
		exportButtonPanel.add(ListAllDepartments);
		exportButtonPanel.add(ListAllPositions);
		exportButtonPanel.add(ListMorning);
		exportButtonPanel.add(ListEvening);
		exportButtonPanel.add(ListAfternoon);
		exportButtonPanel.setSize(301, 452);
		exportButtonPanel.setLocation(90, 5);

		JPanel ExportPanel = new JPanel();
		ExportPanel.setLayout(null);
		TF.setBounds(401, 410, 700, 47);
		ExportPanel.add(TF);
		TableofDBContents2.setPreferredScrollableViewportSize(new Dimension(900, 300));

		dbContentsPanel2=new JScrollPane(TableofDBContents2,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		dbContentsPanel2.setBackground(Color.lightGray);
		dbContentsPanel2.setBorder(BorderFactory.createTitledBorder(lineBorder,"Database Content"));
		((TitledBorder) dbContentsPanel2.getBorder()).setTitleFont(new Font("Arial", Font.ITALIC, 22));
		dbContentsPanel2.setSize(700, 394);
		dbContentsPanel2.setLocation(401, 5);
		
		ExportPanel.add(exportButtonPanel);
		ExportPanel.add(dbContentsPanel2);	

		cp.add(ExportPanel);
		textField.setBounds(0, 0, 289, 47);
		
		ExportPanel.add(textField);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setSize(1000,600);
	}
	

	}

